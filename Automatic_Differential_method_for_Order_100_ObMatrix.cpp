/*  Using Tadiff package to get the Taylor coeffiecients and then calculate the Lie derivative, and finally we could retrieve observability matrix

*/



#include "tadiff.h"
#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include <string>

using namespace std;
using namespace fadbad;


//Reading file mattest.mat...





class FullOrder
{
public:
	T<double> x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25, x26, x27, x28, x29, x30, x31, x32, x33, x34, x35, x36, x37, x38, x39, x40, x41, x42, x43, x44, x45, x46, x47, x48, x49, x50, x51, x52, x53, x54, x55, x56, x57, x58, x59, x60, x61, x62, x63, x64, x65, x66, x67, x68, x69, x70, x71, x72, x73, x74, x75, x76, x77, x78, x79, x80, x81, x82, x83, x84, x85, x86, x87, x88, x89, x90, x91, x92, x93, x94, x95, x96, x97, x98, x99, x100;           // Independent variables
	T<double> xp1, xp2, xp3, xp4, xp5, xp6, xp7, xp8, xp9, xp10, xp11, xp12, xp13, xp14, xp15, xp16, xp17, xp18, xp19, xp20, xp21, xp22, xp23, xp24, xp25, xp26, xp27, xp28, xp29, xp30, xp31, xp32, xp33, xp34, xp35, xp36, xp37, xp38, xp39, xp40, xp41, xp42, xp43, xp44, xp45, xp46, xp47, xp48, xp49, xp50, xp51, xp52, xp53, xp54, xp55, xp56, xp57, xp58, xp59, xp60, xp61, xp62, xp63, xp64, xp65, xp66, xp67, xp68, xp69, xp70, xp71, xp72, xp73, xp74, xp75, xp76, xp77, xp78, xp79, xp80, xp81, xp82, xp83, xp84, xp85, xp86, xp87, xp88, xp89, xp90, xp91, xp92, xp93, xp94, xp95, xp96, xp97, xp98, xp99, xp100;       // Dependent variables
	FullOrder()
	{
	// record DAG at construction:A dependent variable is a variable which has been assigned to another
//dependent variable or to an expression in which variables occured.


//        Code of following part is repeated around 100 times, just written using python.

                xp1 = 41*x2 - 82*x1 + x2*(800*x1 - 800*x2) - x1*(1600*x1 - 800*x2);
                xp2 = 41*x1 - 82*x2 + 41*x3 + x1*(800*x1 - 800*x2) - x2*(800*x1 - 800*x3) + x3*(800*x2 - 800*x3);
                xp3 = 41*x2 - 82*x3 + 41*x4 + x2*(800*x2 - 800*x3) - x3*(800*x2 - 800*x4) + x4*(800*x3 - 800*x4);
                xp4 = 41*x3 - 82*x4 + 41*x5 + x3*(800*x3 - 800*x4) - x4*(800*x3 - 800*x5) + x5*(800*x4 - 800*x5);
                xp5 =  41*x4 - 82*x5 + 41*x6 + x4*(800*x4 - 800*x5) - x5*(800*x4 - 800*x6) + x6*(800*x5 - 800*x6);
                xp6 =  41*x5 - 82*x6 + 41*x7 + x5*(800*x5 - 800*x6) - x6*(800*x5 - 800*x7) + x7*(800*x6 - 800*x7);
                xp7 =  41*x6 - 82*x7 + 41*x8 + x6*(800*x6 - 800*x7) - x7*(800*x6 - 800*x8) + x8*(800*x7 - 800*x8);
                xp8 =  41*x7 - 82*x8 + 41*x9 + x7*(800*x7 - 800*x8) - x8*(800*x7 - 800*x9) + x9*(800*x8 - 800*x9);
             xp9 = 41*x8 - 82*x9 + 41*x10 + x8*(800*x8 - 800*x9) - x9*(800*x8 - 800*x10) + x10*(800*x9 - 800*x10);
       xp10 =   41*x9 - 82*x10 + 41*x11 + x9*(800*x9 - 800*x10) - x10*(800*x9 - 800*x11) + x11*(800*x10 - 800*x11);
    xp11 =  41*x10 - 82*x11 + 41*x12 + x10*(800*x10 - 800*x11) - x11*(800*x10 - 800*x12) + x12*(800*x11 - 800*x12);
    xp12 =  41*x11 - 82*x12 + 41*x13 + x11*(800*x11 - 800*x12) - x12*(800*x11 - 800*x13) + x13*(800*x12 - 800*x13);
    xp13 =  41*x12 - 82*x13 + 41*x14 + x12*(800*x12 - 800*x13) - x13*(800*x12 - 800*x14) + x14*(800*x13 - 800*x14);
    xp14 =   41*x13 - 82*x14 + 41*x15 + x13*(800*x13 - 800*x14) - x14*(800*x13 - 800*x15) + x15*(800*x14 - 800*x15);
    xp15 =  41*x14 - 82*x15 + 41*x16 + x14*(800*x14 - 800*x15) - x15*(800*x14 - 800*x16) + x16*(800*x15 - 800*x16);
    xp16 =  41*x15 - 82*x16 + 41*x17 + x15*(800*x15 - 800*x16) - x16*(800*x15 - 800*x17) + x17*(800*x16 - 800*x17);
    xp17 =  41*x16 - 82*x17 + 41*x18 + x16*(800*x16 - 800*x17) - x17*(800*x16 - 800*x18) + x18*(800*x17 - 800*x18);
    xp18 =  41*x17 - 82*x18 + 41*x19 + x17*(800*x17 - 800*x18) - x18*(800*x17 - 800*x19) + x19*(800*x18 - 800*x19);
    xp19 =  41*x18 - 82*x19 + 41*x20 + x18*(800*x18 - 800*x19) - x19*(800*x18 - 800*x20) + x20*(800*x19 - 800*x20);
    xp20 =  41*x19 - 82*x20 + 41*x21 + x19*(800*x19 - 800*x20) - x20*(800*x19 - 800*x21) + x21*(800*x20 - 800*x21);
    xp21 =  41*x20 - 82*x21 + 41*x22 + x20*(800*x20 - 800*x21) - x21*(800*x20 - 800*x22) + x22*(800*x21 - 800*x22);
    xp22 =  41*x21 - 82*x22 + 41*x23 + x21*(800*x21 - 800*x22) - x22*(800*x21 - 800*x23) + x23*(800*x22 - 800*x23);
    xp23 =  41*x22 - 82*x23 + 41*x24 + x22*(800*x22 - 800*x23) - x23*(800*x22 - 800*x24) + x24*(800*x23 - 800*x24);
    xp24 =  41*x23 - 82*x24 + 41*x25 + x23*(800*x23 - 800*x24) - x24*(800*x23 - 800*x25) + x25*(800*x24 - 800*x25);
    xp25 =  41*x24 - 82*x25 + 41*x26 + x24*(800*x24 - 800*x25) - x25*(800*x24 - 800*x26) + x26*(800*x25 - 800*x26);
    xp26 =  41*x25 - 82*x26 + 41*x27 + x25*(800*x25 - 800*x26) - x26*(800*x25 - 800*x27) + x27*(800*x26 - 800*x27);
    xp27 =  41*x26 - 82*x27 + 41*x28 + x26*(800*x26 - 800*x27) - x27*(800*x26 - 800*x28) + x28*(800*x27 - 800*x28);
    xp28 =  41*x27 - 82*x28 + 41*x29 + x27*(800*x27 - 800*x28) - x28*(800*x27 - 800*x29) + x29*(800*x28 - 800*x29);
    xp29 =  41*x28 - 82*x29 + 41*x30 + x28*(800*x28 - 800*x29) - x29*(800*x28 - 800*x30) + x30*(800*x29 - 800*x30);
    xp30 =  41*x29 - 82*x30 + 41*x31 + x29*(800*x29 - 800*x30) - x30*(800*x29 - 800*x31) + x31*(800*x30 - 800*x31);
    xp31 =  41*x30 - 82*x31 + 41*x32 + x30*(800*x30 - 800*x31) - x31*(800*x30 - 800*x32) + x32*(800*x31 - 800*x32);
    xp32 =  41*x31 - 82*x32 + 41*x33 + x31*(800*x31 - 800*x32) - x32*(800*x31 - 800*x33) + x33*(800*x32 - 800*x33);
    xp33 =  41*x32 - 82*x33 + 41*x34 + x32*(800*x32 - 800*x33) - x33*(800*x32 - 800*x34) + x34*(800*x33 - 800*x34);
    xp34 =  41*x33 - 82*x34 + 41*x35 + x33*(800*x33 - 800*x34) - x34*(800*x33 - 800*x35) + x35*(800*x34 - 800*x35);
    xp35 =  41*x34 - 82*x35 + 41*x36 + x34*(800*x34 - 800*x35) - x35*(800*x34 - 800*x36) + x36*(800*x35 - 800*x36);
    xp36 =  41*x35 - 82*x36 + 41*x37 + x35*(800*x35 - 800*x36) - x36*(800*x35 - 800*x37) + x37*(800*x36 - 800*x37);
    xp37 =  41*x36 - 82*x37 + 41*x38 + x36*(800*x36 - 800*x37) - x37*(800*x36 - 800*x38) + x38*(800*x37 - 800*x38);
    xp38 =  41*x37 - 82*x38 + 41*x39 + x37*(800*x37 - 800*x38) - x38*(800*x37 - 800*x39) + x39*(800*x38 - 800*x39);
    xp39 =  41*x38 - 82*x39 + 41*x40 + x38*(800*x38 - 800*x39) - x39*(800*x38 - 800*x40) + x40*(800*x39 - 800*x40);
    xp40 =  41*x39 - 82*x40 + 41*x41 + x39*(800*x39 - 800*x40) - x40*(800*x39 - 800*x41) + x41*(800*x40 - 800*x41);
    xp41 = 41*x40 - 82*x41 + 41*x42 + x40*(800*x40 - 800*x41) - x41*(800*x40 - 800*x42) + x42*(800*x41 - 800*x42);
    xp42 =  41*x41 - 82*x42 + 41*x43 + x41*(800*x41 - 800*x42) - x42*(800*x41 - 800*x43) + x43*(800*x42 - 800*x43);
    xp43 =  41*x42 - 82*x43 + 41*x44 + x42*(800*x42 - 800*x43) - x43*(800*x42 - 800*x44) + x44*(800*x43 - 800*x44);
    xp44 =  41*x43 - 82*x44 + 41*x45 + x43*(800*x43 - 800*x44) - x44*(800*x43 - 800*x45) + x45*(800*x44 - 800*x45);
    xp45 =  41*x44 - 82*x45 + 41*x46 + x44*(800*x44 - 800*x45) - x45*(800*x44 - 800*x46) + x46*(800*x45 - 800*x46);
    xp46 =  41*x45 - 82*x46 + 41*x47 + x45*(800*x45 - 800*x46) - x46*(800*x45 - 800*x47) + x47*(800*x46 - 800*x47);
    xp47 =  41*x46 - 82*x47 + 41*x48 + x46*(800*x46 - 800*x47) - x47*(800*x46 - 800*x48) + x48*(800*x47 - 800*x48);
    xp48 =  41*x47 - 82*x48 + 41*x49 + x47*(800*x47 - 800*x48) - x48*(800*x47 - 800*x49) + x49*(800*x48 - 800*x49);
    xp49 =  41*x48 - 82*x49 + 41*x50 + x48*(800*x48 - 800*x49) - x49*(800*x48 - 800*x50) + x50*(800*x49 - 800*x50);
    xp50 =  41*x49 - 82*x50 + 41*x51 + x49*(800*x49 - 800*x50) - x50*(800*x49 - 800*x51) + x51*(800*x50 - 800*x51);
    xp51 =  41*x50 - 82*x51 + 41*x52 + x50*(800*x50 - 800*x51) - x51*(800*x50 - 800*x52) + x52*(800*x51 - 800*x52);
    xp52 = 41*x51 - 82*x52 + 41*x53 + x51*(800*x51 - 800*x52) - x52*(800*x51 - 800*x53) + x53*(800*x52 - 800*x53);
    xp53 =  41*x52 - 82*x53 + 41*x54 + x52*(800*x52 - 800*x53) - x53*(800*x52 - 800*x54) + x54*(800*x53 - 800*x54);
    xp54 =  41*x53 - 82*x54 + 41*x55 + x53*(800*x53 - 800*x54) - x54*(800*x53 - 800*x55) + x55*(800*x54 - 800*x55);
    xp55 =  41*x54 - 82*x55 + 41*x56 + x54*(800*x54 - 800*x55) - x55*(800*x54 - 800*x56) + x56*(800*x55 - 800*x56);
    xp56 =  41*x55 - 82*x56 + 41*x57 + x55*(800*x55 - 800*x56) - x56*(800*x55 - 800*x57) + x57*(800*x56 - 800*x57);
    xp57 =  41*x56 - 82*x57 + 41*x58 + x56*(800*x56 - 800*x57) - x57*(800*x56 - 800*x58) + x58*(800*x57 - 800*x58);
    xp58 =  41*x57 - 82*x58 + 41*x59 + x57*(800*x57 - 800*x58) - x58*(800*x57 - 800*x59) + x59*(800*x58 - 800*x59);
    xp59 =  41*x58 - 82*x59 + 41*x60 + x58*(800*x58 - 800*x59) - x59*(800*x58 - 800*x60) + x60*(800*x59 - 800*x60);
    xp60 =  41*x59 - 82*x60 + 41*x61 + x59*(800*x59 - 800*x60) - x60*(800*x59 - 800*x61) + x61*(800*x60 - 800*x61);
    xp61 =  41*x60 - 82*x61 + 41*x62 + x60*(800*x60 - 800*x61) - x61*(800*x60 - 800*x62) + x62*(800*x61 - 800*x62);
    xp62 =  41*x61 - 82*x62 + 41*x63 + x61*(800*x61 - 800*x62) - x62*(800*x61 - 800*x63) + x63*(800*x62 - 800*x63);
    xp63 =  41*x62 - 82*x63 + 41*x64 + x62*(800*x62 - 800*x63) - x63*(800*x62 - 800*x64) + x64*(800*x63 - 800*x64);
    xp64 =  41*x63 - 82*x64 + 41*x65 + x63*(800*x63 - 800*x64) - x64*(800*x63 - 800*x65) + x65*(800*x64 - 800*x65);
    xp65 =  41*x64 - 82*x65 + 41*x66 + x64*(800*x64 - 800*x65) - x65*(800*x64 - 800*x66) + x66*(800*x65 - 800*x66);
    xp66 =  41*x65 - 82*x66 + 41*x67 + x65*(800*x65 - 800*x66) - x66*(800*x65 - 800*x67) + x67*(800*x66 - 800*x67);
    xp67 =  41*x66 - 82*x67 + 41*x68 + x66*(800*x66 - 800*x67) - x67*(800*x66 - 800*x68) + x68*(800*x67 - 800*x68);
    xp68 =  41*x67 - 82*x68 + 41*x69 + x67*(800*x67 - 800*x68) - x68*(800*x67 - 800*x69) + x69*(800*x68 - 800*x69);
    xp69 =  41*x68 - 82*x69 + 41*x70 + x68*(800*x68 - 800*x69) - x69*(800*x68 - 800*x70) + x70*(800*x69 - 800*x70);
    xp70 =  41*x69 - 82*x70 + 41*x71 + x69*(800*x69 - 800*x70) - x70*(800*x69 - 800*x71) + x71*(800*x70 - 800*x71);
    xp71 =  41*x70 - 82*x71 + 41*x72 + x70*(800*x70 - 800*x71) - x71*(800*x70 - 800*x72) + x72*(800*x71 - 800*x72);
    xp72 =  41*x71 - 82*x72 + 41*x73 + x71*(800*x71 - 800*x72) - x72*(800*x71 - 800*x73) + x73*(800*x72 - 800*x73);
    xp73 =  41*x72 - 82*x73 + 41*x74 + x72*(800*x72 - 800*x73) - x73*(800*x72 - 800*x74) + x74*(800*x73 - 800*x74);
    xp74 =  41*x73 - 82*x74 + 41*x75 + x73*(800*x73 - 800*x74) - x74*(800*x73 - 800*x75) + x75*(800*x74 - 800*x75);
    xp75 =  41*x74 - 82*x75 + 41*x76 + x74*(800*x74 - 800*x75) - x75*(800*x74 - 800*x76) + x76*(800*x75 - 800*x76);
    xp76 =  41*x75 - 82*x76 + 41*x77 + x75*(800*x75 - 800*x76) - x76*(800*x75 - 800*x77) + x77*(800*x76 - 800*x77);
    xp77 =  41*x76 - 82*x77 + 41*x78 + x76*(800*x76 - 800*x77) - x77*(800*x76 - 800*x78) + x78*(800*x77 - 800*x78);
    xp78 =  41*x77 - 82*x78 + 41*x79 + x77*(800*x77 - 800*x78) - x78*(800*x77 - 800*x79) + x79*(800*x78 - 800*x79);
    xp79 =  41*x78 - 82*x79 + 41*x80 + x78*(800*x78 - 800*x79) - x79*(800*x78 - 800*x80) + x80*(800*x79 - 800*x80);
    xp80 =  41*x79 - 82*x80 + 41*x81 + x79*(800*x79 - 800*x80) - x80*(800*x79 - 800*x81) + x81*(800*x80 - 800*x81);
    xp81 =  41*x80 - 82*x81 + 41*x82 + x80*(800*x80 - 800*x81) - x81*(800*x80 - 800*x82) + x82*(800*x81 - 800*x82);
    xp82 =  41*x81 - 82*x82 + 41*x83 + x81*(800*x81 - 800*x82) - x82*(800*x81 - 800*x83) + x83*(800*x82 - 800*x83);
    xp83 =  41*x82 - 82*x83 + 41*x84 + x82*(800*x82 - 800*x83) - x83*(800*x82 - 800*x84) + x84*(800*x83 - 800*x84);
    xp84 =  41*x83 - 82*x84 + 41*x85 + x83*(800*x83 - 800*x84) - x84*(800*x83 - 800*x85) + x85*(800*x84 - 800*x85);
    xp85 =  41*x84 - 82*x85 + 41*x86 + x84*(800*x84 - 800*x85) - x85*(800*x84 - 800*x86) + x86*(800*x85 - 800*x86);
    xp86 =  41*x85 - 82*x86 + 41*x87 + x85*(800*x85 - 800*x86) - x86*(800*x85 - 800*x87) + x87*(800*x86 - 800*x87);
    xp87 = 41*x86 - 82*x87 + 41*x88 + x86*(800*x86 - 800*x87) - x87*(800*x86 - 800*x88) + x88*(800*x87 - 800*x88);
    xp88 =  41*x87 - 82*x88 + 41*x89 + x87*(800*x87 - 800*x88) - x88*(800*x87 - 800*x89) + x89*(800*x88 - 800*x89);
    xp89 =  41*x88 - 82*x89 + 41*x90 + x88*(800*x88 - 800*x89) - x89*(800*x88 - 800*x90) + x90*(800*x89 - 800*x90);
     xp90 = 41*x89 - 82*x90 + 41*x91 + x89*(800*x89 - 800*x90) - x90*(800*x89 - 800*x91) + x91*(800*x90 - 800*x91);
     xp91 = 41*x90 - 82*x91 + 41*x92 + x90*(800*x90 - 800*x91) - x91*(800*x90 - 800*x92) + x92*(800*x91 - 800*x92);
     xp92 = 41*x91 - 82*x92 + 41*x93 + x91*(800*x91 - 800*x92) - x92*(800*x91 - 800*x93) + x93*(800*x92 - 800*x93);
     xp93 = 41*x92 - 82*x93 + 41*x94 + x92*(800*x92 - 800*x93) - x93*(800*x92 - 800*x94) + x94*(800*x93 - 800*x94);
     xp94 = 41*x93 - 82*x94 + 41*x95 + x93*(800*x93 - 800*x94) - x94*(800*x93 - 800*x95) + x95*(800*x94 - 800*x95);
     xp95 =  41*x94 - 82*x95 + 41*x96 + x94*(800*x94 - 800*x95) - x95*(800*x94 - 800*x96) + x96*(800*x95 - 800*x96);
     xp96 = 41*x95 - 82*x96 + 41*x97 + x95*(800*x95 - 800*x96) - x96*(800*x95 - 800*x97) + x97*(800*x96 - 800*x97);
     xp97 = 41*x96 - 82*x97 + 41*x98 + x96*(800*x96 - 800*x97) - x97*(800*x96 - 800*x98) + x98*(800*x97 - 800*x98);
     xp98 = 41*x97 - 82*x98 + 41*x99 + x97*(800*x97 - 800*x98) - x98*(800*x97 - 800*x99) + x99*(800*x98 - 800*x99);
    xp99 = 41*x98 - 82*x99 + 41*x100 + x98*(800*x98 - 800*x99) - x99*(800*x98 - 800*x100) + x100*(800*x99 - 800*x100);
    xp100 =                             41*x99 - 41*x100 + x99*(800*x99 - 800*x100) - x100*(800*x99 - 800*x100);






	}



    void reset()
	{

        xp1.reset();
        xp2.reset();
        xp3.reset();
        xp4.reset();
        xp5.reset();
        xp6.reset();
        xp7.reset();
        xp8.reset();
        xp9.reset();
        xp10.reset();
        xp11.reset();
        xp12.reset();
        xp13.reset();
        xp14.reset();
        xp15.reset();
        xp16.reset();
        xp17.reset();
        xp18.reset();
        xp19.reset();
        xp20.reset();
        xp21.reset();
        xp22.reset();
        xp23.reset();
        xp24.reset();
        xp25.reset();
        xp26.reset();
        xp27.reset();
        xp28.reset();
        xp29.reset();
        xp30.reset();
        xp31.reset();
        xp32.reset();
        xp33.reset();
        xp34.reset();
        xp35.reset();
        xp36.reset();
        xp37.reset();
        xp38.reset();
        xp39.reset();
        xp40.reset();
        xp41.reset();
        xp42.reset();
        xp43.reset();
        xp44.reset();
        xp45.reset();
        xp46.reset();
        xp47.reset();
        xp48.reset();
        xp49.reset();
        xp50.reset();
        xp51.reset();
        xp52.reset();
        xp53.reset();
        xp54.reset();
        xp55.reset();
        xp56.reset();
        xp57.reset();
        xp58.reset();
        xp59.reset();
        xp60.reset();
        xp61.reset();
        xp62.reset();
        xp63.reset();
        xp64.reset();
        xp65.reset();
        xp66.reset();
        xp67.reset();
        xp68.reset();
        xp69.reset();
        xp70.reset();
        xp71.reset();
        xp72.reset();
        xp73.reset();
        xp74.reset();
        xp75.reset();
        xp76.reset();
        xp77.reset();
        xp78.reset();
        xp79.reset();
        xp80.reset();
        xp81.reset();
        xp82.reset();
        xp83.reset();
        xp84.reset();
        xp85.reset();
        xp86.reset();
        xp87.reset();
        xp88.reset();
        xp89.reset();
        xp90.reset();
        xp91.reset();
        xp92.reset();
        xp93.reset();
        xp94.reset();
        xp95.reset();
        xp96.reset();
        xp97.reset();
        xp98.reset();
        xp99.reset();
        xp100.reset();
	}
};







int main()
{



    const char* data[201][100];
    string num1;
    float x;
    
    // Get the states value at each time from matlab calculations
    
    std::ifstream input("data_100Order.txt");

   // float x = strtof(cc, NULL);


    for (int i = 0; i < 201; i++) {
            for (int j = 0; j < 100;j++) {
        input >> num1;
         //cout << num1 <<endl;
        data[i][j] = num1.c_str();
        
        
        //data[i][j]=strtof((num1).c_str(),0) ;
        cout << data[i][j] <<endl;
        }
    };


	// Construct ODE:
    
	FullOrder ode;

	// Set point of expansion:
    
        ode.x1[0]=strtof(data[0][0],NULL);   //not working
        ode.x2[0]=0;
        ode.x3[0]=0;
        ode.x4[0]=0;
        ode.x5[0]=0;
        ode.x6[0]=0;
        ode.x7[0]=0;
        ode.x8[0]=0;
        ode.x9[0]=0;
        ode.x10[0]=0;
        ode.x11[0]=0;
        ode.x12[0]=0;
        ode.x13[0]=0;
        ode.x14[0]=0;
        ode.x15[0]=0;
        ode.x16[0]=0;
        ode.x17[0]=0;
        ode.x18[0]=0;
        ode.x19[0]=0;
        ode.x20[0]=0;
        ode.x21[0]=0;
        ode.x22[0]=0;
        ode.x23[0]=0;
        ode.x24[0]=0;
        ode.x25[0]=0;
        ode.x26[0]=0;
        ode.x27[0]=0;
        ode.x28[0]=0;
        ode.x29[0]=0;
        ode.x30[0]=0;
        ode.x31[0]=0;
        ode.x32[0]=0;
        ode.x33[0]=0;
        ode.x34[0]=0;
        ode.x35[0]=0;
        ode.x36[0]=0;
        ode.x37[0]=0;
        ode.x38[0]=0;
        ode.x39[0]=0;
        ode.x40[0]=0;
        ode.x41[0]=0;
        ode.x42[0]=0;
        ode.x43[0]=0;
        ode.x44[0]=0;
        ode.x45[0]=0;
        ode.x46[0]=0;
        ode.x47[0]=0;
        ode.x48[0]=0;
        ode.x49[0]=0;
        ode.x50[0]=0;
        ode.x51[0]=0;
        ode.x52[0]=0;
        ode.x53[0]=0;
        ode.x54[0]=0;
        ode.x55[0]=0;
        ode.x56[0]=0;
        ode.x57[0]=0;
        ode.x58[0]=0;
        ode.x59[0]=0;
        ode.x60[0]=0;
        ode.x61[0]=0;
        ode.x62[0]=0;
        ode.x63[0]=0;
        ode.x64[0]=0;
        ode.x65[0]=0;
        ode.x66[0]=0;
        ode.x67[0]=0;
        ode.x68[0]=0;
        ode.x69[0]=0;
        ode.x70[0]=0;
        ode.x71[0]=0;
        ode.x72[0]=0;
        ode.x73[0]=0;
        ode.x74[0]=0;
        ode.x75[0]=0;
        ode.x76[0]=0;
        ode.x77[0]=0;
        ode.x78[0]=0;
        ode.x79[0]=0;
        ode.x80[0]=0;
        ode.x81[0]=0;
        ode.x82[0]=0;
        ode.x83[0]=0;
        ode.x84[0]=0;
        ode.x85[0]=0;
        ode.x86[0]=0;
        ode.x87[0]=0;
        ode.x88[0]=0;
        ode.x89[0]=0;
        ode.x90[0]=0;
        ode.x91[0]=0;
        ode.x92[0]=0;
        ode.x93[0]=0;
        ode.x94[0]=0;
        ode.x95[0]=0;
        ode.x96[0]=0;
        ode.x97[0]=0;
        ode.x98[0]=0;
        ode.x99[0]=0;
        ode.x100[0]=0;


	int i;
	for(i=0;i<100;i++)
	{
	// Evaluate the i'th Taylor coefficient of
	// the r.h.s. of the ODE:
    
        ode.xp1.eval(i);
        ode.xp2.eval(i);
        ode.xp3.eval(i);
        ode.xp4.eval(i);
        ode.xp5.eval(i);
        ode.xp6.eval(i);
        ode.xp7.eval(i);
        ode.xp8.eval(i);
        ode.xp9.eval(i);
        ode.xp10.eval(i);
        ode.xp11.eval(i);
        ode.xp12.eval(i);
        ode.xp13.eval(i);
        ode.xp14.eval(i);
        ode.xp15.eval(i);
        ode.xp16.eval(i);
        ode.xp17.eval(i);
        ode.xp18.eval(i);
        ode.xp19.eval(i);
        ode.xp20.eval(i);
        ode.xp21.eval(i);
        ode.xp22.eval(i);
        ode.xp23.eval(i);
        ode.xp24.eval(i);
        ode.xp25.eval(i);
        ode.xp26.eval(i);
        ode.xp27.eval(i);
        ode.xp28.eval(i);
        ode.xp29.eval(i);
        ode.xp30.eval(i);
        ode.xp31.eval(i);
        ode.xp32.eval(i);
        ode.xp33.eval(i);
        ode.xp34.eval(i);
        ode.xp35.eval(i);
        ode.xp36.eval(i);
        ode.xp37.eval(i);
        ode.xp38.eval(i);
        ode.xp39.eval(i);
        ode.xp40.eval(i);
        ode.xp41.eval(i);
        ode.xp42.eval(i);
        ode.xp43.eval(i);
        ode.xp44.eval(i);
        ode.xp45.eval(i);
        ode.xp46.eval(i);
        ode.xp47.eval(i);
        ode.xp48.eval(i);
        ode.xp49.eval(i);
        ode.xp50.eval(i);
        ode.xp51.eval(i);
        ode.xp52.eval(i);
        ode.xp53.eval(i);
        ode.xp54.eval(i);
        ode.xp55.eval(i);
        ode.xp56.eval(i);
        ode.xp57.eval(i);
        ode.xp58.eval(i);
        ode.xp59.eval(i);
        ode.xp60.eval(i);
        ode.xp61.eval(i);
        ode.xp62.eval(i);
        ode.xp63.eval(i);
        ode.xp64.eval(i);
        ode.xp65.eval(i);
        ode.xp66.eval(i);
        ode.xp67.eval(i);
        ode.xp68.eval(i);
        ode.xp69.eval(i);
        ode.xp70.eval(i);
        ode.xp71.eval(i);
        ode.xp72.eval(i);
        ode.xp73.eval(i);
        ode.xp74.eval(i);
        ode.xp75.eval(i);
        ode.xp76.eval(i);
        ode.xp77.eval(i);
        ode.xp78.eval(i);
        ode.xp79.eval(i);
        ode.xp80.eval(i);
        ode.xp81.eval(i);
        ode.xp82.eval(i);
        ode.xp83.eval(i);
        ode.xp84.eval(i);
        ode.xp85.eval(i);
        ode.xp86.eval(i);
        ode.xp87.eval(i);
        ode.xp88.eval(i);
        ode.xp89.eval(i);
        ode.xp90.eval(i);
        ode.xp91.eval(i);
        ode.xp92.eval(i);
        ode.xp93.eval(i);
        ode.xp94.eval(i);
        ode.xp95.eval(i);
        ode.xp96.eval(i);
        ode.xp97.eval(i);
        ode.xp98.eval(i);
        ode.xp99.eval(i);
        ode.xp100.eval(i);

	// Since x_i = z_(i-1)/i we have
    
            ode.x1[i+1]=ode.xp1[i]/double(i+1);
            ode.x2[i+1]=ode.xp2[i]/double(i+1);
            ode.x3[i+1]=ode.xp3[i]/double(i+1);
            ode.x4[i+1]=ode.xp4[i]/double(i+1);
            ode.x5[i+1]=ode.xp5[i]/double(i+1);
            ode.x6[i+1]=ode.xp6[i]/double(i+1);
            ode.x7[i+1]=ode.xp7[i]/double(i+1);
            ode.x8[i+1]=ode.xp8[i]/double(i+1);
            ode.x9[i+1]=ode.xp9[i]/double(i+1);
            ode.x10[i+1]=ode.xp10[i]/double(i+1);
            ode.x11[i+1]=ode.xp11[i]/double(i+1);
            ode.x12[i+1]=ode.xp12[i]/double(i+1);
            ode.x13[i+1]=ode.xp13[i]/double(i+1);
            ode.x14[i+1]=ode.xp14[i]/double(i+1);
            ode.x15[i+1]=ode.xp15[i]/double(i+1);
            ode.x16[i+1]=ode.xp16[i]/double(i+1);
            ode.x17[i+1]=ode.xp17[i]/double(i+1);
            ode.x18[i+1]=ode.xp18[i]/double(i+1);
            ode.x19[i+1]=ode.xp19[i]/double(i+1);
            ode.x20[i+1]=ode.xp20[i]/double(i+1);
            ode.x21[i+1]=ode.xp21[i]/double(i+1);
            ode.x22[i+1]=ode.xp22[i]/double(i+1);
            ode.x23[i+1]=ode.xp23[i]/double(i+1);
            ode.x24[i+1]=ode.xp24[i]/double(i+1);
            ode.x25[i+1]=ode.xp25[i]/double(i+1);
            ode.x26[i+1]=ode.xp26[i]/double(i+1);
            ode.x27[i+1]=ode.xp27[i]/double(i+1);
            ode.x28[i+1]=ode.xp28[i]/double(i+1);
            ode.x29[i+1]=ode.xp29[i]/double(i+1);
            ode.x30[i+1]=ode.xp30[i]/double(i+1);
            ode.x31[i+1]=ode.xp31[i]/double(i+1);
            ode.x32[i+1]=ode.xp32[i]/double(i+1);
            ode.x33[i+1]=ode.xp33[i]/double(i+1);
            ode.x34[i+1]=ode.xp34[i]/double(i+1);
            ode.x35[i+1]=ode.xp35[i]/double(i+1);
            ode.x36[i+1]=ode.xp36[i]/double(i+1);
            ode.x37[i+1]=ode.xp37[i]/double(i+1);
            ode.x38[i+1]=ode.xp38[i]/double(i+1);
            ode.x39[i+1]=ode.xp39[i]/double(i+1);
            ode.x40[i+1]=ode.xp40[i]/double(i+1);
            ode.x41[i+1]=ode.xp41[i]/double(i+1);
            ode.x42[i+1]=ode.xp42[i]/double(i+1);
            ode.x43[i+1]=ode.xp43[i]/double(i+1);
            ode.x44[i+1]=ode.xp44[i]/double(i+1);
            ode.x45[i+1]=ode.xp45[i]/double(i+1);
            ode.x46[i+1]=ode.xp46[i]/double(i+1);
            ode.x47[i+1]=ode.xp47[i]/double(i+1);
            ode.x48[i+1]=ode.xp48[i]/double(i+1);
            ode.x49[i+1]=ode.xp49[i]/double(i+1);
            ode.x50[i+1]=ode.xp50[i]/double(i+1);
            ode.x51[i+1]=ode.xp51[i]/double(i+1);
            ode.x52[i+1]=ode.xp52[i]/double(i+1);
            ode.x53[i+1]=ode.xp53[i]/double(i+1);
            ode.x54[i+1]=ode.xp54[i]/double(i+1);
            ode.x55[i+1]=ode.xp55[i]/double(i+1);
            ode.x56[i+1]=ode.xp56[i]/double(i+1);
            ode.x57[i+1]=ode.xp57[i]/double(i+1);
            ode.x58[i+1]=ode.xp58[i]/double(i+1);
            ode.x59[i+1]=ode.xp59[i]/double(i+1);
            ode.x60[i+1]=ode.xp60[i]/double(i+1);
            ode.x61[i+1]=ode.xp61[i]/double(i+1);
            ode.x62[i+1]=ode.xp62[i]/double(i+1);
            ode.x63[i+1]=ode.xp63[i]/double(i+1);
            ode.x64[i+1]=ode.xp64[i]/double(i+1);
            ode.x65[i+1]=ode.xp65[i]/double(i+1);
            ode.x66[i+1]=ode.xp66[i]/double(i+1);
            ode.x67[i+1]=ode.xp67[i]/double(i+1);
            ode.x68[i+1]=ode.xp68[i]/double(i+1);
            ode.x69[i+1]=ode.xp69[i]/double(i+1);
            ode.x70[i+1]=ode.xp70[i]/double(i+1);
            ode.x71[i+1]=ode.xp71[i]/double(i+1);
            ode.x72[i+1]=ode.xp72[i]/double(i+1);
            ode.x73[i+1]=ode.xp73[i]/double(i+1);
            ode.x74[i+1]=ode.xp74[i]/double(i+1);
            ode.x75[i+1]=ode.xp75[i]/double(i+1);
            ode.x76[i+1]=ode.xp76[i]/double(i+1);
            ode.x77[i+1]=ode.xp77[i]/double(i+1);
            ode.x78[i+1]=ode.xp78[i]/double(i+1);
            ode.x79[i+1]=ode.xp79[i]/double(i+1);
            ode.x80[i+1]=ode.xp80[i]/double(i+1);
            ode.x81[i+1]=ode.xp81[i]/double(i+1);
            ode.x82[i+1]=ode.xp82[i]/double(i+1);
            ode.x83[i+1]=ode.xp83[i]/double(i+1);
            ode.x84[i+1]=ode.xp84[i]/double(i+1);
            ode.x85[i+1]=ode.xp85[i]/double(i+1);
            ode.x86[i+1]=ode.xp86[i]/double(i+1);
            ode.x87[i+1]=ode.xp87[i]/double(i+1);
            ode.x88[i+1]=ode.xp88[i]/double(i+1);
            ode.x89[i+1]=ode.xp89[i]/double(i+1);
            ode.x90[i+1]=ode.xp90[i]/double(i+1);
            ode.x91[i+1]=ode.xp91[i]/double(i+1);
            ode.x92[i+1]=ode.xp92[i]/double(i+1);
            ode.x93[i+1]=ode.xp93[i]/double(i+1);
            ode.x94[i+1]=ode.xp94[i]/double(i+1);
            ode.x95[i+1]=ode.xp95[i]/double(i+1);
            ode.x96[i+1]=ode.xp96[i]/double(i+1);
            ode.x97[i+1]=ode.xp97[i]/double(i+1);
            ode.x98[i+1]=ode.xp98[i]/double(i+1);
            ode.x99[i+1]=ode.xp99[i]/double(i+1);
            ode.x100[i+1]=ode.xp100[i]/double(i+1);
	}

	// Print out the Taylor coefficients for the solution
	// of the ODE:
    
	for(i=0;i<=100;i++)
	{
        cout << "x1[" << i << "]=" << ode.x1[i] << endl;
        cout << "x2[" << i << "]=" << ode.x2[i] << endl;
        cout << "x3[" << i << "]=" << ode.x3[i] << endl;
        cout << "x4[" << i << "]=" << ode.x4[i] << endl;
        cout << "x5[" << i << "]=" << ode.x5[i] << endl;
        cout << "x6[" << i << "]=" << ode.x6[i] << endl;
        cout << "x7[" << i << "]=" << ode.x7[i] << endl;
        cout << "x8[" << i << "]=" << ode.x8[i] << endl;
        cout << "x9[" << i << "]=" << ode.x9[i] << endl;
        cout << "x10[" << i << "]=" << ode.x10[i] << endl;
        cout << "x11[" << i << "]=" << ode.x11[i] << endl;
        cout << "x12[" << i << "]=" << ode.x12[i] << endl;
        cout << "x13[" << i << "]=" << ode.x13[i] << endl;
        cout << "x14[" << i << "]=" << ode.x14[i] << endl;
        cout << "x15[" << i << "]=" << ode.x15[i] << endl;
        cout << "x16[" << i << "]=" << ode.x16[i] << endl;
        cout << "x17[" << i << "]=" << ode.x17[i] << endl;
        cout << "x18[" << i << "]=" << ode.x18[i] << endl;
        cout << "x19[" << i << "]=" << ode.x19[i] << endl;
        cout << "x20[" << i << "]=" << ode.x20[i] << endl;
        cout << "x21[" << i << "]=" << ode.x21[i] << endl;
        cout << "x22[" << i << "]=" << ode.x22[i] << endl;
        cout << "x23[" << i << "]=" << ode.x23[i] << endl;
        cout << "x24[" << i << "]=" << ode.x24[i] << endl;
        cout << "x25[" << i << "]=" << ode.x25[i] << endl;
        cout << "x26[" << i << "]=" << ode.x26[i] << endl;
        cout << "x27[" << i << "]=" << ode.x27[i] << endl;
        cout << "x28[" << i << "]=" << ode.x28[i] << endl;
        cout << "x29[" << i << "]=" << ode.x29[i] << endl;
        cout << "x30[" << i << "]=" << ode.x30[i] << endl;
        cout << "x31[" << i << "]=" << ode.x31[i] << endl;
        cout << "x32[" << i << "]=" << ode.x32[i] << endl;
        cout << "x33[" << i << "]=" << ode.x33[i] << endl;
        cout << "x34[" << i << "]=" << ode.x34[i] << endl;
        cout << "x35[" << i << "]=" << ode.x35[i] << endl;
        cout << "x36[" << i << "]=" << ode.x36[i] << endl;
        cout << "x37[" << i << "]=" << ode.x37[i] << endl;
        cout << "x38[" << i << "]=" << ode.x38[i] << endl;
        cout << "x39[" << i << "]=" << ode.x39[i] << endl;
        cout << "x40[" << i << "]=" << ode.x40[i] << endl;
        cout << "x41[" << i << "]=" << ode.x41[i] << endl;
        cout << "x42[" << i << "]=" << ode.x42[i] << endl;
        cout << "x43[" << i << "]=" << ode.x43[i] << endl;
        cout << "x44[" << i << "]=" << ode.x44[i] << endl;
        cout << "x45[" << i << "]=" << ode.x45[i] << endl;
        cout << "x46[" << i << "]=" << ode.x46[i] << endl;
        cout << "x47[" << i << "]=" << ode.x47[i] << endl;
        cout << "x48[" << i << "]=" << ode.x48[i] << endl;
        cout << "x49[" << i << "]=" << ode.x49[i] << endl;
        cout << "x50[" << i << "]=" << ode.x50[i] << endl;
        cout << "x51[" << i << "]=" << ode.x51[i] << endl;
        cout << "x52[" << i << "]=" << ode.x52[i] << endl;
        cout << "x53[" << i << "]=" << ode.x53[i] << endl;
        cout << "x54[" << i << "]=" << ode.x54[i] << endl;
        cout << "x55[" << i << "]=" << ode.x55[i] << endl;
        cout << "x56[" << i << "]=" << ode.x56[i] << endl;
        cout << "x57[" << i << "]=" << ode.x57[i] << endl;
        cout << "x58[" << i << "]=" << ode.x58[i] << endl;
        cout << "x59[" << i << "]=" << ode.x59[i] << endl;
        cout << "x60[" << i << "]=" << ode.x60[i] << endl;
        cout << "x61[" << i << "]=" << ode.x61[i] << endl;
        cout << "x62[" << i << "]=" << ode.x62[i] << endl;
        cout << "x63[" << i << "]=" << ode.x63[i] << endl;
        cout << "x64[" << i << "]=" << ode.x64[i] << endl;
        cout << "x65[" << i << "]=" << ode.x65[i] << endl;
        cout << "x66[" << i << "]=" << ode.x66[i] << endl;
        cout << "x67[" << i << "]=" << ode.x67[i] << endl;
        cout << "x68[" << i << "]=" << ode.x68[i] << endl;
        cout << "x69[" << i << "]=" << ode.x69[i] << endl;
        cout << "x70[" << i << "]=" << ode.x70[i] << endl;
        cout << "x71[" << i << "]=" << ode.x71[i] << endl;
        cout << "x72[" << i << "]=" << ode.x72[i] << endl;
        cout << "x73[" << i << "]=" << ode.x73[i] << endl;
        cout << "x74[" << i << "]=" << ode.x74[i] << endl;
        cout << "x75[" << i << "]=" << ode.x75[i] << endl;
        cout << "x76[" << i << "]=" << ode.x76[i] << endl;
        cout << "x77[" << i << "]=" << ode.x77[i] << endl;
        cout << "x78[" << i << "]=" << ode.x78[i] << endl;
        cout << "x79[" << i << "]=" << ode.x79[i] << endl;
        cout << "x80[" << i << "]=" << ode.x80[i] << endl;
        cout << "x81[" << i << "]=" << ode.x81[i] << endl;
        cout << "x82[" << i << "]=" << ode.x82[i] << endl;
        cout << "x83[" << i << "]=" << ode.x83[i] << endl;
        cout << "x84[" << i << "]=" << ode.x84[i] << endl;
        cout << "x85[" << i << "]=" << ode.x85[i] << endl;
        cout << "x86[" << i << "]=" << ode.x86[i] << endl;
        cout << "x87[" << i << "]=" << ode.x87[i] << endl;
        cout << "x88[" << i << "]=" << ode.x88[i] << endl;
        cout << "x89[" << i << "]=" << ode.x89[i] << endl;
        cout << "x90[" << i << "]=" << ode.x90[i] << endl;
        cout << "x91[" << i << "]=" << ode.x91[i] << endl;
        cout << "x92[" << i << "]=" << ode.x92[i] << endl;
        cout << "x93[" << i << "]=" << ode.x93[i] << endl;
        cout << "x94[" << i << "]=" << ode.x94[i] << endl;
        cout << "x95[" << i << "]=" << ode.x95[i] << endl;
        cout << "x96[" << i << "]=" << ode.x96[i] << endl;
        cout << "x97[" << i << "]=" << ode.x97[i] << endl;
        cout << "x98[" << i << "]=" << ode.x98[i] << endl;
        cout << "x99[" << i << "]=" << ode.x99[i] << endl;
        cout << "x100[" << i << "]=" << ode.x100[i] << endl;

	}



// Save the data into documents to be used by Matlab

 /*   ofstream abcdef;
    abcdef.open("C:/C:\Users\Brandon\Documents\C_Practice\Lie.bin",ios::out | ios::trunc | ios::binary);

    for (int i=0; i<10; i++)
    {
        float x_cord;
        x_cord = i*1.38;
        float y_cord;
        y_cord = i*10;
        abcdef<<x_cord<<"   "<<y_cord<<endl;
    }

    abcdef.close();*/

 //   In Matlab use   " data = load('test.bin');  "
    return 0;
};



