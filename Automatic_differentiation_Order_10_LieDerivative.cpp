#include "tadiff.h"
#include "badiff.h"
#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include <string>

using namespace std;
using namespace fadbad;

//matdgns mattest.mat
//Reading file mattest.mat...


class FullOrder
{
public:
	T<double> x1, x2, x3, x4, x5, x6, x7, x8, x9, x10;
	T<double> xp1, xp2, xp3, xp4, xp5, xp6, xp7, xp8, xp9, xp10;
    FullOrder()
	{
	// record DAG at construction:A dependent variable is a variable which has been assigned to another
//dependent variable or to an expression in which variables occured (i.e. a
//non-constant expression).

                xp1 = 41*x2 - 82*x1 + x2*(800*x1 - 800*x2) - x1*(1600*x1 - 800*x2);
                xp2 = 41*x1 - 82*x2 + 41*x3 + x1*(800*x1 - 800*x2) - x2*(800*x1 - 800*x3) + x3*(800*x2 - 800*x3);
                xp3 = 41*x2 - 82*x3 + 41*x4 + x2*(800*x2 - 800*x3) - x3*(800*x2 - 800*x4) + x4*(800*x3 - 800*x4);
                xp4 = 41*x3 - 82*x4 + 41*x5 + x3*(800*x3 - 800*x4) - x4*(800*x3 - 800*x5) + x5*(800*x4 - 800*x5);
                xp5 =  41*x4 - 82*x5 + 41*x6 + x4*(800*x4 - 800*x5) - x5*(800*x4 - 800*x6) + x6*(800*x5 - 800*x6);
                xp6 =  41*x5 - 82*x6 + 41*x7 + x5*(800*x5 - 800*x6) - x6*(800*x5 - 800*x7) + x7*(800*x6 - 800*x7);
                xp7 =  41*x6 - 82*x7 + 41*x8 + x6*(800*x6 - 800*x7) - x7*(800*x6 - 800*x8) + x8*(800*x7 - 800*x8);
                xp8 =  41*x7 - 82*x8 + 41*x9 + x7*(800*x7 - 800*x8) - x8*(800*x7 - 800*x9) + x9*(800*x8 - 800*x9);
                xp9 = 41*x8 - 82*x9 + 41*x10 + x8*(800*x8 - 800*x9) - x9*(800*x8 - 800*x10) + x10*(800*x9 - 800*x10);
                xp10 =                             41*x9 - 41*x10 + x9*(800*x9 - 800*x10) - x10*(800*x9 - 800*x10);



	}
	void reset()
	{

        xp1.reset();
        xp2.reset();
        xp3.reset();
        xp4.reset();
        xp5.reset();
        xp6.reset();
        xp7.reset();
        xp8.reset();
        xp9.reset();
        xp10.reset();

	}
};






int main(int argc, char * argv[])
{
    std::fstream myfile("ddd.txt", std::ios_base::in);           // read the data from matlab

    float a[10][100];
    float c[100][100];

    for (int i=1;i<=10;i++){
        for (int j=1;j<=100;j++){
            myfile >> a[i][j];
        }
    }



	FullOrder ode;

	// Set point of expansion:

            for (int j=0;j<=99;j++){

                ode.x1[0]=a[1][j];
                ode.x2[0]=a[2][j];
                ode.x3[0]=a[3][j];
                ode.x4[0]=a[4][j];
                ode.x5[0]=a[5][j];
                ode.x6[0]=a[6][j];
                ode.x7[0]=a[7][j];
                ode.x8[0]=a[8][j];
                ode.x9[0]=a[9][j];
                ode.x10[0]=a[10][j];


	int i;
	for(i=0;i<10;i++)
	{
	// Evaluate the i'th Taylor coefficient of
	// the r.h.s. of the ODE:
        ode.xp1.eval(i);
        ode.xp2.eval(i);
        ode.xp3.eval(i);
        ode.xp4.eval(i);
        ode.xp5.eval(i);
        ode.xp6.eval(i);
        ode.xp7.eval(i);
        ode.xp8.eval(i);
        ode.xp9.eval(i);
        ode.xp10.eval(i);


	// Since d(x,y,z,p)/dt=lorenz(x,y,z,p) we have

            ode.x1[i+1]=ode.xp1[i]/double(i+1);
            ode.x2[i+1]=ode.xp2[i]/double(i+1);
            ode.x3[i+1]=ode.xp3[i]/double(i+1);
            ode.x4[i+1]=ode.xp4[i]/double(i+1);
            ode.x5[i+1]=ode.xp5[i]/double(i+1);
            ode.x6[i+1]=ode.xp6[i]/double(i+1);
            ode.x7[i+1]=ode.xp7[i]/double(i+1);
            ode.x8[i+1]=ode.xp8[i]/double(i+1);
            ode.x9[i+1]=ode.xp9[i]/double(i+1);
            ode.x10[i+1]=ode.xp10[i]/double(i+1);

	}

    for(int k=0;k<10;k++)
        {
        int m = k*10;
        c[m+1][j]= ode.x1[k];
        c[m+2][j]= ode.x2[k];
        c[m+3][j]= ode.x3[k];
        c[m+4][j]= ode.x4[k];
        c[m+5][j]= ode.x5[k];	// Print out the Taylor coefficients for the solution
        c[m+6][j]= ode.x6[k];
        c[m+7][j]= ode.x7[k];
        c[m+8][j]= ode.x8[k];
        c[m+9][j]= ode.x9[k];
        c[m+10][j]= ode.x10[k];
        }




// Save the data into documents to be used by Matlab
  const int size = 100;

  ofstream myfile ("Lie.txt");
  if (myfile.is_open())
  {
    for(int i=0; i<size; i++){
            for (int j=0;j<size;j++){
        myfile << c[i][j] << " " ;
    }
  }
   myfile.close();
  } else cout << "Unable to open file";


 //   In Matlab use " importdata" function
    return 0;

};
};


