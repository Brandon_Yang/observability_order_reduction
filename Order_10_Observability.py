# Try to get the observability matrix for 10th order electrical system, no order reduction

import numpy as np
import numdifftools as nd
import sympy as sp
from array import array
from numpy.linalg import inv
from sympy import MatrixSymbol, Matrix
from sympy import *
from sympy import Symbol
import sys
sys.stdout=open("OrderReduction_10.txt","w")

# FUll order system with Nth orders, and try to get the state equations

def System_Order(N):
    X = MatrixSymbol('X', N, 1) 
    A = np.zeros((N,N))          
    A[0][0] = -82
    A[0][1] = 41
    A[N-1][N-2] = 41
    A[N-1][N-1] = -41
    for i in range(1,N-1):
        A[i][i-1] = 41
        A[i][i] = -82
        A[i][i+1] = 41
    W = np.zeros((N,N,N))
    W[0:2,0:2,0] = [[-1600,800],[800,-800]]
    for i in range(1,N-1):
        W[i-1:i+2,i-1:i+2,i] = [[800,-800,0],[-800,0,800],[0,800,-800]]
    W[N-2:N,N-2:N,N-1] = [[800,-800],[-800,800]]
    B = np.zeros((N,1))
    B[0] = 1
    C = np.zeros((N,1))
    C[0] = 1
    x = Symbol('x')
    WX = ones(1,N) * x

    #WX = np.zeros((100,1))
    for i in range(0,N):
	bbd = Matrix(X).transpose()*W[:,:,i]*Matrix(X)
	WX[i] = Matrix(bbd)[0,0]	
	#WX[i]=WX[i].subs(WX[i],Matrix(bbd)[0,0])
	#WX[i] = np.multiply(np.multiply(X,W[:,:,i-1]),X.transpose())
    y = A*Matrix(X) + WX.transpose()            #   state equation f(x)
    return y

# Lie derivative function to get the observability matrix

def Lie_matrix(h0, X, N):
    #K = MatrixSymbol('x', N, N)
    N = 10
    cc = Symbol('cc')
    K = ones(N,N) * cc
    K[0,0:N] = zeros(1,N)
    K[0,0] = 1                    # Output the first state
    for i in range(1,N):
        E = K[i-1,:]*h0
       # E = sp.Matrix(E)
        K[i,:] = E.jacobian(X)    # ith line of observability matrix
    return K                


# Main function to compile all the functions together to get the observability variation of systems with different orders
 

N = 10    # Order of the system
X = MatrixSymbol('X', N, 1)
h0 = System_Order(N)
h = Lie_matrix(h0, X, N)          # h is the observability matrix
print(h)

sys.stdout.close()