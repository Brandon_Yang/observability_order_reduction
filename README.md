# README #

All the code is about order reduction for the large scale system, including both Matlab and Python code.

### What is this repository for? ###

It covers examples like 100 order circuits to do the order reduction based on Quadratic method


### How do I get set up? ###

For the CPP code, it used the automatic differentiation method, which includes dependent variables and independent variables:
Dependent variables are variables whose values are results of expressiones in which variables occur.
Independent variables are variables which are not dependent, i.e. variables whose values has been assigned to constants or expressions in which only constants occurred.