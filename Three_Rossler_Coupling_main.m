% Rossler coupling for two cases:
% SSS(All oscillators have interaction through similar variables) and
% DDD(All oscillators have interaction through dissimilar varibales)


%% DDD coupling


%t = 0:0.01:5;

% x10 = zeros(9,1);
% C = zeros(9,1);
% C(1) = 1;           % Initial conditions
% 
% [t,x6] = ode45( 'DDD_Coupling',t,x10); 
% y1 = C'*x6';  
%    
% 
% Ord = 5;
% x10 = 0.01*ones(9,1);             % Initial conditions
% [Ar Cr V] = Output_DDD(Ord);        % use function Output_matrix to get Cr 
% x20 = V'*x10;
% [t,x2] = ode45( 'DDD_Coupling_Reduced',t,x20); 
% y2 = Cr*x2';  
% 
%     plot(t,y1,t,y2);
%     legend('Full order','Reduced order');
%     xlabel('Simulation time(s)','fontsize',24);
%     ylabel('Response','fontsize',24);
%     xt = get(gca, 'XTick');
%     set(gca, 'FontSize', 20);
%     grid on;
% 
%




%% SSS coupling
% Output comparison between full order and reduced order
% 
% figure;
clc;
clear;
t = 0:0.01:5;  % Time 
x30 = 0.01*ones(9,1);         %Initial conditions
C = zeros(9,1);               % C matrix
C(1) = 1;
[t,x3] = ode45( 'SSS_Coupling',t,x30); 
y3 = C'*x3';  


M = size(t,1);
y4 = zeros(M,3);

for i = 1:3
set = [7,5,3];           % Order set is even
%set = [8,6,4];          % Order set is odd


Ord = set(i);
%Initial conditions

[Ar Cr V] = Output_SSS(Ord);        % use function Output_matrix to get Cr and V 

x60 = V'*x30;
[t,x4] = ode45( 'SSS_Coupling_Reduced',t,x60); 
y4(:,i) = Cr*x4';                   % Output of reduced order systems
end





% Plots for the 3d trajectories
% plot3(x3(:,1),x3(:,4),x3(:,7));
% xlabel('x1','fontsize',24);
% ylabel('x2','fontsize',24);
% zlabel('x3','fontsize',24);
% grid on;



plot(t,y3,t,y4(:,1),t,y4(:,2),t,y4(:,3));
%legend('9th order','8th order','6th order','4th order');
legend('9th order','7th order','5th order','3rd order');
xlabel('Simulation time(s)','fontsize',24);
ylabel('Response','fontsize',24);

grid on;
    
    
%     
%     
%     
%     
%     
    
%     
%     
%%  Observability comparisons


% tspan  = 0:0.05:5;   % time scale
% M = size(tspan,2);
% N = 9;
% X = sym('x',[N 1]);
% 
% % Calculate the Lie derivative matrix
% 
% 
% m = 0.005*ones(N,1);
% x0 = double(m);
% 
% 
% 
% x30 = 0.01*ones(9,1);     
% C = zeros(9,1);
% C(1) = 1;
% [t,y] = ode45( 'SSS_Coupling',tspan,x30); 
% %y = C'*x3';  
%    
% 
% h0 = SSS_Coupling_Symbolic(tspan,X);
% h = Lie_matrix(h0,X,N);           % Get the symbolic Lie derivative
% % [t,y] = ode23( 'SSS_Coupling',tspan,x0);
% 
% 
% 
% OA = zeros(M,1);           % Full order observability
% 
% for j = 1:M
%     old = digits(5);
%     Observ = vpa(subs(h,X,y(j,:)'));        % reduce the accuracy for calculation
%     Eign = eig(Observ'*Observ);
%     OA(j) = abs(min(Eign)/max(Eign));
% end
% 
% 
% OB = zeros(M,3);                  % Full order observability
% % -----------------------------------------------------------------------------------------
% for i = 1:7
%     
% set = [8,7,6,5,4,3,2];            %Order set
% N = 9;   
% Ord = set(i)
% 
% X = sym('x',[Ord 1]);
% 
% 
% % Initial conditions
% [Ar Cr V] = Output_SSS(Ord);        % use function Output_matrix to get Cr 
% x60 = V'*x30;
% [t,z] = ode45( 'SSS_Coupling_Reduced',tspan,x60); 
% %z = Cr*x4';  
% 
% 
% h0 = SSS_Coupling_Reduced_Symbolic(tspan,X);
% h = Lie_matrix(h0,X,Ord);
% % [t,z] = ode23( 'SSS_Coupling_Reduced',tspan,x0);
% 


%%
%================================================================================
% M = size(t,1);
% 
% 
% % Based on the observability matrix, we calculate the matrix eigenvalue
% % ratio of min/max with time variation
% 
% 
% 
% 
% for j = 1:M
%     old = digits(5);
%     Observ = vpa(subs(h,X,z(j,:)'));        % reduce the accuracy for calculation
%     Eign = eig(Observ'*Observ);
%     OB(j,i) = abs(min(Eign)/max(Eign));
% end
% 
% 
% end
% 
% semilogy(t,OA(:),t,OB(:,1),t,OB(:,2),t,OB(:,3));
% legend('Full order','5th order','3rd order','2nd order');
% xlabel('Simulation time','fontsize',24);
% ylabel('Observability metric','fontsize',24);
% % 
% 
% % 
% % 
% % 
% 
% 
% 
